crudGenerator
============

$ php artisan migrate   // basic migrate to create users

$ php artisan db:seed --class=UserSeeder    // Seed demo User :

Email : admin@admin.com
Password : admin123456

crudGenerator commands :

$ php artisan  generateCrud  <option>  <model>  <json>

option :

  migration :   Create migrations    (database/migrations/)
  
  model :       Create Model         (app/Models/)
  
  controller :  Create Controller    (app/Http/Controllers) 
  
  form :        Create form          (resources/views/)
  
  view :        Create views         (resources/views/)
  
  all :         Create all the above .
 

Exp :

$ php artisan  generateCrud  all  Post  examplesJson/post2.json

$ php artisan  generateCrud  all  Categorie  examplesJson/categories.json



Source Code  :             app/Console/Commands/generateCrud.php

Components Templates    :  resources/views/templates/