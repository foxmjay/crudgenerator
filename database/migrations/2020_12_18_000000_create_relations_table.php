<?php 
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('relations', function (Blueprint $table) {
                     $table->id();
                     $table->string('name',255);
                     $table->string('field',255);
                     $table->enum('type',['belongsTo','hasMany'])->nullable();
                     $table->string('fkClass',255);
                     $table->string('fkField',255);
                     $table->string('displayField',255);
                 });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('relations');
    }
}

 ?>